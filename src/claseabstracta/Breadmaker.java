package claseabstracta;

/**
 *
 * @author Carlos Páez
 */
public class Breadmaker extends Electrodomesticos {
    
    private String boton1;
    private String boton2;

    public Breadmaker(String nombre, int tamanio, String marca, String color) {
        super(nombre, tamanio, marca, color);
        this.boton1="a";
        this.boton1="b";
    }
        public String getBoton1() {
        return boton1;
    }

    public void setBoton1(String boton1) {
        this.boton1 = boton1;
    }

    public String getBoton2() {
        return boton2;
    }

    public void setBoton2(String boton2) {
        this.boton2 = boton2;
    }
    
}
