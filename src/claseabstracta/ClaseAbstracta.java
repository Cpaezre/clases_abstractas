
package claseabstracta;
/*
 * @author Carlos Páez
 */
public class ClaseAbstracta {

    public static void main(String[] args) {
        // TODO code application logic here
        
        
        Electrodomesticos [] electro = new Electrodomesticos [3];
        electro [0] = new Nevera ("Nevecon", 50, "Samsung", "Blanco");
        electro [1] = new Televisor ("TV LG", 49, "LG", "Negro");
        electro [2] = new Breadmaker("Breadmaker", 12, "Sunbeam", "Plateado");
        
        for(Electrodomesticos e: electro){
            System.out.println("Nombre: " + e.getNombre()+"\nTamaño en pulgadas: "+e.getTamanio()+"\nMarca: "+e.getMarca()+"\nColor: "+e.getColor()+"\n");
        }        
    }       
}