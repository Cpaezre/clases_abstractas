package claseabstracta;
/**
 * @author Carlos Páez
 */
public abstract class Electrodomesticos {
    private String nombre;
    private int tamanio;
    private String marca;
    private String color;

    public Electrodomesticos(String nombre, int tamanio, String marca, String color) {
        this.nombre = nombre;
        this.tamanio = tamanio;
        this.marca = marca;
        this.color = color;
    }
    
    public Electrodomesticos() {
    }
    
    //public abstract void asignaValor();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTamanio() {
        return tamanio;
    }

    public void setTamanio(int tamanio) {
        this.tamanio = tamanio;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }   
}