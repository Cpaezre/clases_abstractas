package claseabstracta;
/**
 * @author Carlos Páez
 */
public class Nevera extends Electrodomesticos {

    public Nevera(String nombre, int tamanio, String marca, String color) {
        super(nombre, tamanio, marca, color);
    }
    
    public Nevera(){
    }
    
}
